-- https://wiki.haskell.org/Implement_a_chat_server
{-# LANGUAGE
    ViewPatterns
#-}
module Main where

--import Control.Concurrent
import Control.Exception (SomeException, catch)
import Data.Time
import Network.Socket
import System.IO
import System.Random

-- | (lower boundary, upper boundary) of the number of trials
trials :: (Int, Int)
trials = (5, 22)

timeout :: Int
timeout = 10

main :: IO ()
main = do
  nums <- randoms <$> getStdGen
  sock <- socket AF_INET Stream 0           -- create socket
  setSocketOption sock ReuseAddr 1          -- make socket immediately reusable
  bind sock (SockAddrInet 4242 iNADDR_ANY)  -- listen on TCP port 4242.
  listen sock 2                             -- set a max of 2 queued connection
  mainLoop nums sock

mainLoop :: [Int] -> Socket -> IO ()
mainLoop nums sock = do
  conn <- accept sock
  n' <- runConn nums conn                  -- no @forkIO@ for simplicity 
  mainLoop n' sock

runConn :: [Int] -> (Socket, SockAddr) -> IO [Int]
runConn nums (sock, _) = do
  putStrLn "accepted a new client."
  hdl <- socketToHandle sock ReadWriteMode
  hSetBuffering hdl NoBuffering
  hPutStrLn hdl "Hello. I'm Sphinx of AdDP-2017."
  let loop :: Int -> [Int] -> IO [Int]
      loop ((0 <) -> False) ls = do
        hPutStrLn hdl "0 0"
        putStrLn =<< hGetLine hdl
        return ls
      loop i l = do
        let [a64, b64] = take 2 l
            a = abs $ mod a64 4096 + 1
            b = abs $ mod b64 4096 + 1
        ta <- getCurrentTime
        hPutStrLn hdl $ show a ++ " " ++ show b
        s <- hGetLine hdl
        tb <- getCurrentTime
        let td :: Int
            td = ceiling (diffUTCTime tb ta)
        let c = read s          -- it might emit an error, 'can't convert to an Int'
        if c == a + b && td < timeout
          then loop (i - 1) $ drop 2 l
          else do let msg = if td < timeout then "A wrong sum" else "Too late"
                  hPutStrLn hdl msg
                  putStrLn msg
                  hClose hdl
                  return $ drop 2 l
  let rep = (fst trials) + mod (abs (head nums)) (snd trials - fst trials)
  catch
    (loop rep (tail nums))
    (\msg -> do putStrLn $ "error: " ++ show (msg :: SomeException)
                hClose hdl
                return (tail nums))
